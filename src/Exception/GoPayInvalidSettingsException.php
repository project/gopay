<?php

namespace Drupal\gopay\Exception;

/**
 * Class GoPayInvalidSettingsException.
 *
 * @package Drupal\gopay\Exception
 */
class GoPayInvalidSettingsException extends GoPayException {

}
