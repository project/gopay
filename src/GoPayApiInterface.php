<?php

namespace Drupal\gopay;

use Drupal\gopay\Payment\PaymentInterface;

/**
 * Interface GoPayApiInterface.
 *
 * @package Drupal\gopay
 */
interface GoPayApiInterface {

  /**
   * Sets the GoPay config.
   *
   * @param string $go_id
   *   GoId.
   * @param string $client_id
   *   ClientId.
   * @param string $client_secret
   *   ClientSecret.
   * @param string $is_production_mode
   *   Is production mode?
   */
  public function setGoPayConfig($go_id, $client_id, $client_secret, $is_production_mode);

  /**
   * Creates \GoPay\Payments object based on configuration.
   *
   * @param array $user_config
   *   GoPay compatible configuration.
   * @param array $user_services
   *   List additional of services.
   *
   * @return \GoPay\Payments
   *   Payments object.
   */
  public function config(array $user_config, array $user_services = []);

  /**
   * Tests connection to GoPay.
   *
   * @return array
   *   Array of tested results keyed by:
   *   - token: \GoPay\Token\AccessToken object.
   */
  public function runTests();

  /**
   * Returns available payment methods.
   *
   * @return array
   *   Associative array with keys as machine name of payment method, value can
   *   be human-readable method.
   */
  public function getPaymentInstruments();

  /**
   * Creates link for GoPay gateway.
   *
   * @param \Drupal\gopay\Payment\PaymentInterface $payment
   *   Payment object.
   *
   * @return \GoPay\Http\Response
   *   Response object corresponding to the created GoPay payment.
   */
  public function createGoPayPayment(PaymentInterface $payment);

  /**
   * Creates link for GoPay gateway.
   *
   * @param \Drupal\gopay\Payment\PaymentInterface $payment
   *   Payment object.
   * @param string $text
   *   Text of link.
   *
   * @return array
   *   Render array with GoPay inline form.
   */
  public function buildLink(PaymentInterface $payment, $text = NULL);

  /**
   * Creates inline form as GoPay gateway.
   *
   * @param \Drupal\gopay\Payment\PaymentInterface $payment
   *   Payment object.
   * @param string $text
   *   Text of link.
   *
   * @return array
   *   Render array with GoPay inline form.
   */
  public function buildInlineForm(PaymentInterface $payment, $text = NULL);

  /**
   * Gets Payment status.
   *
   * @param int $id
   *   Payment Id.
   *
   * @return \GoPay\Http\Response
   *   Response object.
   */
  public function getPaymentStatus($id);

}
