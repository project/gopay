<?php

namespace Drupal\gopay\Eet;

/**
 * Class Eet.
 *
 * @package Drupal\gopay\Eet
 */
class Eet {

  protected $outputMapping = [
    'dic_poverujiciho'  => 'dicPoverujiciho',
    'celk_trzba'        => 'celkTrzba',
    'zakl_nepodl_dph'   => 'zaklNepodlDph',
    'zakl_dan1'         => 'zaklDan1',
    'dan1'              => 'dan1',
    'zakl_dan2'         => 'zaklDan2',
    'dan2'              => 'dan2',
    'zakl_dan3'         => 'zaklDan3',
    'dan3'              => 'dan3',
    'cest_sluz'         => 'cestSluz',
    'pouzit_zboz1'      => 'pouzitZboz1',
    'pouzit_zboz2'      => 'pouzitZboz2',
    'pouzit_zboz3'      => 'pouzitZboz3',
    'urceno_cerp_zuct'  => 'urcenoCerpZuct',
    'cerp_zuct'         => 'cerpZuct',
    'mena'              => 'mena',
  ];

  /**
   * DIČ of the entrustment taxpayer.
   *
   * @var string
   */
  protected $dicPoverujiciho;

  /**
   * The total amount.
   *
   * @var int
   */
  protected $celkTrzba;

  /**
   * The total amount of supplies exempt from VAT.
   *
   * @var int
   */
  protected $zaklNepodlDph;

  /**
   * The total tax base amount, the basic VAT rate.
   *
   * @var int
   */
  protected $zaklDan1;

  /**
   * The total amount of VAT, the basic rate.
   *
   * @var int
   */
  protected $dan1;

  /**
   * The total tax base amount with a first reduced rate of VAT.
   *
   * @var int
   */
  protected $zaklDan2;

  /**
   * The total VAT amount with a first reduced rate.
   *
   * @var int
   */
  protected $dan2;

  /**
   * The total tax base amount with a second reduced rate of VAT.
   *
   * @var int
   */
  protected $zaklDan3;

  /**
   * The total VAT amount with a second reduced rate.
   *
   * @var int
   */
  protected $dan3;

  /**
   * The total amount of the VAT regime for travel service.
   *
   * @var int
   */
  protected $cestSluz;

  /**
   * The total amount of the VAT regime for the sale of used goods with a basic rate.
   *
   * @var int
   */
  protected $pouzitZboz1;

  /**
   * The total amount of the VAT regime for the sale of used goods with a first reduced rate.
   *
   * @var int
   */
  protected $pouzitZboz2;

  /**
   * The total amount of the VAT regime for the sale of used goods with a second reduced rate.
   *
   * @var int
   */
  protected $pouzitZboz3;

  /**
   * The total amount of payments designated for subsequent pumping or settlement.
   *
   * @var int
   */
  protected $urcenoCerpZuct;

  /**
   * The total amount of payments which are followed by pumping or settlement of the payment.
   *
   * @var int
   */
  protected $cerpZuct;

  /**
   * Currency of the parameters.
   *
   * @var string
   */
  protected $mena;

  /**
   * Sets DIČ of the entrustment taxpayer.
   *
   * @param string $dicPoverujiciho
   *   DIČ of the entrustment taxpayer.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setDicPoverujiciho($dicPoverujiciho) {
    $this->dicPoverujiciho = $dicPoverujiciho;
    return $this;
  }

  /**
   * Sets the total amount.
   *
   * @param int $celkTrzba
   *   The total amount.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setCelkTrzba($celkTrzba, $inCents = TRUE) {
      $this->setPropertyMonetaryAmount('celkTrzba', $celkTrzba, $inCents);
    return $this;
  }

  /**
   * Sets the total amount of supplies exempt from VAT.
   *
   * @param int $zaklNepodlDph
   *   The total amount of supplies exempt from VAT.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setZaklNepodlDph($zaklNepodlDph, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('zaklNepodlDph', $zaklNepodlDph, $inCents);
    return $this;
  }

  /**
   * Sets the total tax base amount, the basic VAT rate.
   *
   * @param int $zaklDan1
   *   The total tax base amount, the basic VAT rate.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setZaklDan1($zaklDan1, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('zaklDan1', $zaklDan1, $inCents);
    return $this;
  }

  /**
   * Sets the total amount of VAT, the basic rate.
   *
   * @param int $dan1
   *   The total amount of VAT, the basic rate.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setDan1($dan1, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('dan1', $dan1, $inCents);
    return $this;
  }

  /**
   * Sets the total tax base amount with a first reduced rate of VAT.
   *
   * @param int $zaklDan2
   *   The total tax base amount with a first reduced rate of VAT.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setZaklDan2($zaklDan2, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('zaklDan2', $zaklDan2, $inCents);
    return $this;
  }

  /**
   * Sets the total VAT amount with a first reduced rate.
   *
   * @param int $dan2
   *   The total VAT amount with a first reduced rate.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setDan2($dan2, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('dan2', $dan2, $inCents);
    return $this;
  }

  /**
   * Sets the total tax base amount with a second reduced rate of VAT.
   *
   * @param int $zaklDan3
   *   The total tax base amount with a second reduced rate of VAT.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setZaklDan3($zaklDan3, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('zaklDan3', $zaklDan3, $inCents);
    return $this;
  }

  /**
   * Sets the total VAT amount with a second reduced rate.
   *
   * @param int $dan3
   *   The total VAT amount with a second reduced rate.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setDan3($dan3, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('dan3', $dan3, $inCents);
    return $this;
  }

  /**
   * Sets the total amount of the VAT regime for travel service.
   *
   * @param int $cestSluz
   *   The total amount of the VAT regime for travel service.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setCestSluz($cestSluz, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('cestSluz', $cestSluz, $inCents);
    return $this;
  }

  /**
   * Sets the total amount of the VAT regime for the sale of used goods with a basic rate.
   *
   * @param int $pouzitZboz1
   *   The total amount of the VAT regime for the sale of used goods with a basic rate.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setPouzitZboz1($pouzitZboz1, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('pouzitZboz1', $pouzitZboz1, $inCents);
    return $this;
  }

  /**
   * Sets the total amount of the VAT regime for the sale of used goods with a first reduced rate.
   *
   * @param int $pouzitZboz2
   *   The total amount of the VAT regime for the sale of used goods with a first reduced rate.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setPouzitZboz2($pouzitZboz2, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('pouzitZboz2', $pouzitZboz2, $inCents);
    return $this;
  }

  /**
   * Sets the total amount of the VAT regime for the sale of used goods with a second reduced rate..
   *
   * @param int $pouzitZboz3
   *   The total amount of the VAT regime for the sale of used goods with a second reduced rate.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setPouzitZboz3($pouzitZboz3, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('pouzitZboz3', $pouzitZboz3, $inCents);
    return $this;
  }

  /**
   * Sets the total amount of payments designated for subsequent pumping or settlement.
   *
   * @param int $urcenoCerpZuct
   *   The total amount of payments designated for subsequent pumping or settlement.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setUrcenoCerpZuct($urcenoCerpZuct, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('urcenoCerpZuct', $urcenoCerpZuct, $inCents);
    return $this;
  }

  /**
   * Sets the total amount of payments which are followed by pumping or settlement of the payment.
   *
   * @param int $cerpZuct
   *   The total amount of payments which are followed by pumping or settlement of the payment.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setCerpZuct($cerpZuct, $inCents = TRUE) {
    $this->setPropertyMonetaryAmount('cerpZuct', $cerpZuct, $inCents);
    return $this;
  }

  /**
   * Sets currency of the parameters.
   *
   * @param string $mena
   *   Currency of the parameters.
   *
   * @return Eet
   *   Returns itself.
   */
  public function setMena($mena) {
    $this->mena = $mena;
    return $this;
  }

  /**
   * Creates payment configuration compatible with GoPay SDK Payments.
   *
   * This array can be assigned to a Payment object.
   *
   * @return array
   *   Configuration of EET
   */
  public function toArray() {
    $ret = [];

    foreach ($this->outputMapping as $arrayKey => $propertyName) {
      $this->mapPropertyToArray($propertyName, $arrayKey, $ret);
    }

    return $ret;
  }

  /**
   * Maps a given property to the array passed down.
   *
   * @param string $propertyName
   *   The name of the property.
   * @param string $arrayKey
   *   The key in the array.
   * @param array $array
   *   The array.
   */
  protected function mapPropertyToArray($propertyName, $arrayKey, array &$array) {
    $propertyValue = $this->$propertyName;

    if (!empty($propertyValue)) {
      $array[$arrayKey] = $propertyValue;
    }
  }

  /**
   * Assigns a monetary value to a property.
   *
   * @param string $propertyName
   *   The name of the property.
   * @param int $monetaryAmount
   *   The monetary amount to be assigned.
   * @param bool $inCents
   *   Whether $amount is in cents or in units. GoPay API gets amount in cents,
   *   so if you enter amount in units, it will be converted to cents anyway.
   */
  protected function setPropertyMonetaryAmount($propertyName, $monetaryAmount, $inCents = TRUE) {
    if (!$inCents) {
      $monetaryAmount *= 100;
    }
    $this->$propertyName = $monetaryAmount;
  }

}
