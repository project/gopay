<?php

namespace Drupal\gopay;

use Drupal\gopay\Exception\GoPayException;
use GoPay\Api;
use Drupal\Core\Config\ConfigFactoryInterface;
use GoPay\Definition\Payment\PaymentInstrument;
use Drupal\gopay\Payment\PaymentInterface;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class GoPayApi.
 *
 * @package Drupal\gopay
 */
class GoPayApi implements GoPayApiInterface {
  use StringTranslationTrait;

  /**
   * ConfigFactory Service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger Service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactory
   */
  protected $logger;

  /**
   * Payments object created on GoPay init.
   *
   * @var \GoPay\Payments
   */
  protected $goPay;

  /**
   * GoPayApi constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   ConfigFactory Service.
   * @param \Drupal\Core\Logger\LoggerChannelFactory $logger
   *   Logger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerChannelFactory $logger) {
    $this->configFactory = $config_factory;
    $this->logger = $logger;
    $settings = $this->configFactory->get('gopay.settings');

    $this->setGoPayConfig(
      $settings->get('go_id'),
      $settings->get('client_id'),
      $settings->get('client_secret'),
      $settings->get('production_mode')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function setGoPayConfig($go_id, $client_id, $client_secret, $is_production_mode) {
    $this->goPay = $this->config([
      'goid' => $go_id,
      'clientId' => $client_id,
      'clientSecret' => $client_secret,
      'isProductionMode' => $is_production_mode,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function config(array $user_config, array $user_services = []) {
    return Api::payments($user_config, $user_services);
  }

  /**
   * {@inheritdoc}
   */
  public function runTests() {
    $token = $this->goPay->auth->authorize();

    return [
      'token' => $token,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentInstruments() {
    return [
      PaymentInstrument::PAYMENT_CARD => $this->t('Payment card'),
      PaymentInstrument::BANK_ACCOUNT => $this->t('Bank account'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function createGoPayPayment(PaymentInterface $payment) {
    $config = $payment->toArray();

    $response = $this->goPay->createPayment($config);

    if (!$response->hasSucceed()) {
      $this->logger->get('gopay')->error('Unexpected error GoPay API error: ' . $response->__toString());

      throw new GoPayException($response->__toString());
    }

    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public function buildLink(PaymentInterface $payment, $text = NULL) {
    try {
      $goPayPayment = $this->createGoPayPayment($payment);
      $goPayUrl = $goPayPayment->json['gw_url'];
    }
    catch (GoPayException $e) {
      return ['#markup' => $this->t('Unexpected GoPay API error.')];
    }

    if (!$text) {
      $text = $this->t('Pay');
    }

    return [
      '#theme' => 'gopay_link',
      '#link_text' => $text,
      '#gopay_url' => $goPayUrl,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildInlineForm(PaymentInterface $payment, $text = NULL) {
    try {
      $goPayPayment = $this->createGoPayPayment($payment);
      $goPayUrl = $goPayPayment->json['gw_url'];
    }
    catch (GoPayException $e) {
      return ['#markup' => $this->t('Unexpected GoPay API error.')];
    }

    if (!$text) {
      $text = $this->t('Pay');
    }

    return [
      '#theme' => 'gopay_inline_form',
      '#link_text' => $text,
      '#gopay_url' => $goPayUrl,
      '#embed_js' => $this->goPay->urlToEmbedJs(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getPaymentStatus($id) {
    return $this->goPay->getStatus($id);
  }

}
